package com.tb.beanswithannotation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
@Controller
public class User implements ApplicationEventPublisherAware{
	private Sum sumA;
	@Autowired
	private MessageSource messageSource;
	private ApplicationEventPublisher publisher; 
/*	private Sum sumB;
	private Sum sumC;*/
	
	public MessageSource getMessageSource() {
		return messageSource;
	}
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	public Sum getSumA() {
		return sumA;
	}
	@Autowired
	public void setSumA(Sum sumA) {
		this.sumA = sumA;
	}
	/*public Sum getSumB() {
		return sumB;
	}
	@Autowired
	public void setSumB(Sum sumB) {
		this.sumB = sumB;
	}
	public Sum getSumC() {
		return sumC;
	}
	@Autowired
	public void setSumC(Sum sumC) {
		this.sumC = sumC;
	}*/
	public void showSum() {
		System.out.println(this.messageSource.getMessage("messages.sum",null,"Default adding",null));
		System.out.println(this.messageSource.getMessage("sumofuserone",new Object[] {sumA.getX(),sumA.getY()},"Default x,y",null));
		System.out.println(this.messageSource.getMessage("sumof",new Object[] {getSumA().addAll()},"Default sum",null));
		System.out.println(this.messageSource.getMessage("messages",null,"Default sms",null));
		MyEvent event=new MyEvent(this);
		publisher.publishEvent(event);
		
		
	}
	@Override
	public void setApplicationEventPublisher(ApplicationEventPublisher publisher) {
		
		this.publisher=publisher;
		
	}
}
